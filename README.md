# full cycle

full cycle development

- 1 - Design
- 2 - Develop 
    - Frontend
    - Backend
    - Cloud
- 3 - Test
- 4 - Deploy
    - Cloud
    - Containers
    - Orquestradores
- 5 - Operate
- 6 - Support

- Docker
    - https://www.youtube.com/watch?v=39Jl_M3nUTo
